# Abstractions Tabletop Community Bulletin Board

Join our [#tabletop room on Slack](https://abstractions.slack.com/messages/tabletop/).
If you're not on the Slack team, you can get an invite at [http://abstractions.io/chat](http://abstractions.io/chat).

# Abstractions Games List

What games do you want to play with Abstractions attendees?

- [Netrunner](https://boardgamegeek.com/boardgame/124742/android-netrunner)
- [Werewolf](https://boardgamegeek.com/boardgame/38159/ultimate-werewolf-ultimate-edition)
- [Vampire](https://boardgamegeek.com/boardgame/180956/one-night-ultimate-vampire)
- [Coup](https://boardgamegeek.com/boardgame/131357/coup)
- [Escape from the Aliens in Outer Space](https://boardgamegeek.com/boardgame/82168/escape-aliens-outer-space)
- [Magic: the Gathering](https://boardgamegeek.com/boardgame/463/magic-gathering)
- [Betrayal at House on the Hill](https://boardgamegeek.com/boardgame/10547/betrayal-house-hill)
- [Pandemic](https://boardgamegeek.com/boardgame/30549/pandemic)
- [Shadow Over Camelot](https://boardgamegeek.com/boardgame/15062/shadows-over-camelot)

# Gaming Sessions

- ???

# Players

- Justin Reese - [@justinxreese](http://twitter.com/justinxreese). Enjoys: Werewolf, Pandemic, short & silly card games
- Angelique - [@messypixels](http://twitter.com/messypixels). Bringing the following:
    - [Totally Renamed Spy Game](http://www.boardgamegeek.com/boardgame/164/i-kill-you-mister-spy)
    - [Chrononauts](https://boardgamegeek.com/boardgame/815/chrononauts)
    - [Get Bit](https://boardgamegeek.com/boardgame/30539/get-bit)
    - [Gloom](https://boardgamegeek.com/boardgame/12692/gloom)
    - [Red November](https://boardgamegeek.com/boardgame/36946/red-november)
- Ben Hamill - [@benhamill](https://twitter.com/benhamill). Bringing: [Netrunner](https://boardgamegeek.com/boardgame/124742/android-netrunner) unless it sounds like no one else wants to play. Ad hoc games for a mental break in the day FTW.